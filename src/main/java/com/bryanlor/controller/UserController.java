package com.bryanlor.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UserController {
	
	@RequestMapping("/")
	public String index(Model model) {
		model.addAttribute("name", "John Doe");
		return "index";
	}
	
	@RequestMapping("/welcome")
	public String welcome(Model model) {
		model.addAttribute("message", "This is a welcome message.");
		return "welcome";
	}
		
}
